# Changelog

## [1.0.0] - 2024-04-26

### Added

issue #1: Manage flow extensions

## [0.0.1] - 2024-04-26

Initial commit
